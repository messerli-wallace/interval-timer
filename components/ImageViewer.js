import { StyleSheet, Image} from 'react-native';

export default function ImageViewer({ ImageSource }) {
    return (
        <Image source={ImageSource} style={styles.image} />
    );
}

const styles = StyleSheet.create({
  image: {
    width: 120,
    height: 120,
    borderRadius: 18,
  },
});