import React from 'react';
import { StyleSheet, TextInput, View, Text, Pressable } from 'react-native';
import Button from './Button';

const NumberInput = props => {
    const [number, onChangeNumber] = React.useState('');
    return (
        <View>
            <Text style={styles.sentence}>{props.adjacentText}</Text>
            <TextInput 
            style={styles.input}
            onChangeText={onChangeNumber}
            value={number}
            keyboardType='number-pad'
            />
        </View>
    );
};

const Interval = () =>{
    return (
        <View>
        <NumberInput adjacentText="First Time:"/>
        <NumberInput adjacentText="Second TIme:"/>
        </View>
    );
}

const IntervalList = () => {
    const [intervals, setIntervalList] = React.useState([]);

    const AddInterval = () => {
        const newIntervalList = [...intervals];
        newIntervalList.push(<Interval/>);
        setIntervalList(newIntervalList);
    };

    const RemoveInterval = () => {
        const newIntervalList = [...intervals];
        newIntervalList.pop();
        setIntervalList(newIntervalList);
    };

    return (
        <View style={styles.container}>
            <Interval />
            {intervals.map((inte) => inte)}
            <Pressable style={styles.input} onPress={AddInterval}>
                <Text style={styles.sentence}>Add Interval</Text>
            </Pressable>
            <Pressable style={styles.input} onPress={RemoveInterval}>
                <Text style={styles.sentence}>Remove Interval</Text>
            </Pressable>
            <NumberInput adjacentText="Number of reps:"/>            
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        // flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16,
        
    },
    // maybe sentence container
    sentence: {
        fontSize: 16,
        color: '#fff',
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        borderColor: "#fff",
        color: '#fff',
    },
});

export default IntervalList;