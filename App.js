import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, SafeAreaView } from 'react-native';

import Button from './components/Button.js';
import ImageViewer from './components/ImageViewer';
import IntervalList from './components/NumberInput.js';

const icon1 = require('./assets/favicon.png');

export default function App() {
  let firstTime=0;
  let secondTime=0;
  let totalReps=0;
  return (
    <SafeAreaView style={styles.viewContainer}>
    <ScrollView style={styles.scrollContainer}>
      <View style={styles.container}>
        {/* Atempt to make GUI */}
        <IntervalList/>

        {/* <View style={styles.imageContainer}>
          <ImageViewer ImageSource={icon1} />
        </View> */}

        {/* <View style={styles.footerContainer}>
          <Button theme="primary" label="First button"/>
          <Button label="2nd button"/>
        </View> */}
{/* 
        <Text style={{color: '#fff'}}>INTERVAL Timer!</Text> */}
        <StatusBar style="auto" />


      </View>
    </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    paddingTop: 40,
  },
  scrollContainer: {
    marginHorizontal: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#25292e',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageContainer: {
    flex: 1,
    paddingTop: 58,
  },
});
